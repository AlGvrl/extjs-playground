/*
 * This file launches the application by asking Ext JS to create
 * and launch() the Application class.
 */
Ext.application({
    extend: 'PlaygroundApp.Application',

    name: 'PlaygroundApp',

    requires: [
        // This will automatically load all classes in the PlaygroundApp namespace
        // so that application classes do not need to require each other.
        'PlaygroundApp.*'
    ],

    // The name of the initial view to create.
    mainView: 'PlaygroundApp.view.main.Main'
});
