Ext.define('PlaygroundApp.model.Personnel', {
    extend: 'PlaygroundApp.model.Base',

    fields: [
        { name: 'id', type: 'int'}, 
        { name: 'first_name', type: 'string'}, 
        { name: 'last_name', type: 'string'}, 
        // pdf says to convert, but docs recommend using calculate instead
        // Also why is it "fullname" and not "full_name"?
        { 
            name: 'fullname',
            type: 'string',
            calculate: data => `${data.first_name} ${data.last_name}`
        }, 
        { name: 'avatar', type: 'string'}, 
        { name: 'active', type: 'boolean', defaultValue: true}
    ]
});
