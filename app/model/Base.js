Ext.define('PlaygroundApp.model.Base', {
    extend: 'Ext.data.Model',

    schema: {
        namespace: 'PlaygroundApp.model'
    }
});
