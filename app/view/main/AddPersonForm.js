Ext.define('PlaygroundApp.view.main.AddPersonForm', {
    extend: 'Ext.form.Panel',
    xtype: 'form-login',

    header: false,
    bodyPadding: 10,
    defaultType: 'textfield',

    items: [{
        allowBlank: false,
        fieldLabel: 'First name',
        name: 'first_name',
        msgTarget: 'under'
    }, {
        allowBlank: false,
        fieldLabel: 'Last name',
        name: 'last_name',
        msgTarget: 'under'
    }],

    buttons: [
        { 
            text: 'Add',
            handler: 'onAddRecordConfirm',
            formBind: true
        },
        {
            text: 'Close',
            handler: 'onAddRecordClose'
        }
    ],
});