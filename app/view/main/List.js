Ext.define('PlaygroundApp.view.main.List', {
    extend: 'Ext.grid.Panel',
    xtype: 'mainlist',

    disableSelection: true,

    requires: [
        'PlaygroundApp.store.Personnel'
    ],

    title: 'Personnel',

    store: {
        type: 'personnel'
    },

    tbar: [{
        text: 'Add Record',
        handler: 'onAddRecordClick'
    }],

    columns: [
        { 
            dataIndex: 'avatar',
            renderer: function(url) {
                return `<img width=32 height=32 src="${url}" />`;
            },
            width: 48,
            sortable: false,
            hideable: false,
            menuDisabled: true
        },
        { 
            text: 'Name',  
            dataIndex: 'fullname', 
            flex: 1 
        },
        { 
            text: 'Active', 
            xtype: 'checkcolumn', 
            dataIndex: 'active'
        },
        { 
            text: 'Action',
            xtype: 'actioncolumn',
            items: [{
                iconCls: 'x-fa fa-cog',
                tooltip: 'Edit',
                handler: 'onEditClick'
            }, {
                iconCls: 'x-fa fa-minus-circle',
                tooltip: 'Delete',
                handler: 'onDeleteClick'
            }]
        }
    ],

    listeners: {
        itemClick: 'onItemClick'
    }
});
