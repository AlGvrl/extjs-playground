Ext.define('PlaygroundApp.store.Personnel', {
    extend: 'Ext.data.Store',

    alias: 'store.personnel',
    storeId: 'personnel',

    model: 'PlaygroundApp.model.Personnel',

    proxy: {
        type: 'rest',
        url: 'https://reqres.in/api/users',
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
    },
    autoLoad: true
});